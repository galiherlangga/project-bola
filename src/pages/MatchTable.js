import React from 'react';
import {StyleSheet, View} from 'react-native';
import {Container, Header, Content, Text, Body, Title, Form, Picker, Icon} from 'native-base';
import DatePicker from '../component/DatePicker';
import MatchDataTable from '../component/MatchDataTable';

export default class MatchTable extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
          selected: undefined
        };
      }
      onValueChange(value: string) {
        this.setState({
          selected: value
        });
    }
    render(){
        return(
            <Container style={styles.container}>
                <Header searchBar rounded style={{backgroundColor:'#ff6f00'}}>
                    <Body>
                        <Title>Table</Title>
                    </Body>
                </Header>
                <Content contentContainerStyle={styles.content}>
                    <View>
                        <Picker
                            mode="dropdown"
                            iosHeader="Select your SIM"
                            iosIcon={<Icon name="arrow-down" />}
                            style={{ width: undefined, borderWidth:1 }}
                            selectedValue={this.state.selected}
                            onValueChange={this.onValueChange.bind(this)}
                        >
                            <Picker.Item label="Competition" value="key0" />
                            <Picker.Item label="Liga 1 Shopee" value="key1" />
                            <Picker.Item label="Piala Presiden" value="key2" />
                            <Picker.Item label="Kratindeng Piala Indonesia" value="key3" />
                            <Picker.Item label="Trofeo Bali Island Cup" value="key4" />
                            <Picker.Item label="AFC Champion League" value="key5" />
                        </Picker>
                        <DatePicker formatdate="YYYY" rounded={false}/>
                        <MatchDataTable />
                    </View>
                </Content>
            </Container>
        )
    }
}
const styles = StyleSheet.create(
    {
        container:{
            backgroundColor:'#fafafa'
        }
    }
)
import React from 'react';
import {StyleSheet, Image} from 'react-native';
import { Container, Header, Body, Title, Content, View, Tabs, Tab, List, Card, CardItem, Text, Icon, Button } from 'native-base';
import ClubFollowingList from '../component/ClubFollowingList';

import {auth, db} from '../../config/config'

export default class UserNormalProfile extends React.Component{

    constructor(props){

        super(props)
        
        this.state = {
            followings: null,
            userFollowing: null,
            isLoading: true
        }

        this.uid =  auth.currentUser.uid;
    }

    componentDidMount = () => {

        this.getFollowing()
    }

    getFollowing = () => {
        
        db.collection('following').doc(this.uid).get().then(res => {

            this.setState({followings: [res.data()]})
        }).then( async ()=>{
            
            await this.setState({userFollowing: null})

            let following_ids = Object.keys(this.state.followings[0])
            let following_vals = Object.values(this.state.followings[0])
            
            following_ids.forEach((id, index) => {

                if(following_vals[index] == true){

                    db.collection('users').doc(id).get().then(async res => {

                        await this.setState((state, props) => {
                            if(state.userFollowing == null){
                                return ({
                                    userFollowing: [res.data()]
                                })
                            }else{
                                
                                return {userFollowing: [...state.userFollowing, res.data()]}
                            }
                        })

                    })
                }

            })

            // await this.setState({isLoading: false})
        })
    }

    handleFollow = (data) => {
        this.getFollowing()
    }

    handleLogout = ({nav}) => {
        auth.signOut().then(()=>{
            nav.navigate('Auth')
        });
    }

    render(){

        // if(this.state.isLoading){
        //     return null
        // }

        const {nama, email, tanggal_lahir, hp} = this.props.users
        
        return(
            <Container style={styles.container}>
                <Header style={{backgroundColor:'#000'}} androidStatusBarColor="#000">
                    <Body>
                        <Title>Profile</Title>
                    </Body>
                </Header>
                <Content>
                    <View style={styles.head}>
                        <Image source={require('../../assets/avatar.png')} style={styles.avatar}/>
                        <View style={styles.headTextContainer}>
                            <Text>Nama : {nama}</Text>
                            <Text>Email : {email}</Text>
                        </View>

                        <View style={{position: "absolute", bottom: 0, right: 0, flexDirection: "row"}}>
                            <Button style={styles.button} transparent
                                onPress={()=> this.handleLogout(this.props)}
                            >
                                <Icon name='md-power' style={styles.icon}/>
                            </Button>
                        </View>
                    </View>
                    <Tabs>
                        <Tab heading='Following' tabStyle={styles.tabInactive} activeTabStyle={styles.tabActive} textStyle={styles.tabText}>
                            <List>
                                {this.state.userFollowing && (
                                    
                                    this.state.userFollowing.map(following => (
                                        <ClubFollowingList key={following.id} 

                                            isFollow={false} 
                                            dataFollowing={following} 
                                            nama={following.club}
                                            onFollow={state => this.handleFollow(state)}
                                        />
                                    ))
                                )}
                                {/* <ClubFollowingList isFollow={false} nama='Club B'/>
                                <ClubFollowingList isFollow={false} nama='Club C'/>
                                <ClubFollowingList isFollow={false} nama='Club D'/>
                                <ClubFollowingList isFollow={false} nama='Club E'/>
                                <ClubFollowingList isFollow={false} nama='Club F'/> */}
                            </List>
                        </Tab>
                        <Tab heading='About' tabStyle={styles.tabInactive} activeTabStyle={styles.tabActive} textStyle={styles.tabText}>
                            <Card>
                                <CardItem>
                                    <Text>Biodata</Text>
                                </CardItem>
                                <CardItem>
                                    <Body>
                                        <Text>bio</Text>
                                    </Body>
                                </CardItem>
                            </Card>
                            <Card>
                                <CardItem>
                                    <Text>Informasi</Text>
                                </CardItem>
                                <CardItem>
                                    <Body>
                                        <Text>Email : {email}</Text>
                                        <Text>Telepohone : {hp}</Text>
                                        <Text>Tanggal Lahir : {tanggal_lahir}</Text>
                                    </Body>
                                </CardItem>
                            </Card>
                        </Tab>
                    </Tabs>
                </Content>
            </Container>
        )
    }
}

const styles = StyleSheet.create(
    {
        container:{
            backgroundColor:'#fafafa'
        },
        head:{
            flex:0.2,
            flexDirection:'row',
            borderBottomColor: 'black',
            borderBottomWidth: 1,
        },
        avatar:{
            width:120,
            height:120,
            margin:10
        },
        headTextContainer:{
            flex:1,
            flexDirection:'column',
            margin:10
        },
        tabInactive:{
            backgroundColor:'#fff'
        },
        tabActive:{
            backgroundColor:'#000'
        },
        tabText:{
            color:'#000'
        }
    }
)
import React from 'react';
import {StyleSheet} from 'react-native';
import { List } from 'native-base';
import ClubFollowingList from '../component/ClubFollowingList';
import {auth, db} from '../../config/config'

export default class ClubFollowing extends React.Component{

    constructor(props){
        super(props)
        this.state = {
            isLoading: true, 
            followings: null,
            userFollowing: null
        }

        this.uid =  auth.currentUser.uid;
    }

    componentDidMount = () => {

        this.getFollowing()
    }

    getFollowing = () => {
        
        db.collection('following').doc(this.uid).get().then(res => {

            this.setState({followings: [res.data()]})
        }).then( async ()=>{
            
            await this.setState({userFollowing: null})

            let following_ids = Object.keys(this.state.followings[0])
            let following_vals = Object.values(this.state.followings[0])
            
            following_ids.forEach((id, index) => {

                if(following_vals[index] == true){

                    db.collection('users').doc(id).get().then(async res => {

                        await this.setState((state, props) => {
                            if(state.userFollowing == null){
                                return ({
                                    userFollowing: [res.data()]
                                })
                            }else{
                                
                                return {userFollowing: [...state.userFollowing, res.data()]}
                            }
                        })

                    })
                }

            })

            // await this.setState({isLoading: false})
        })
    }

    handleFollow = (data) => {
        this.getFollowing()
    }

    render(){
        // if(this.state.isLoading){
        //     return null
        // }
        return(
            <List>
                {this.state.userFollowing && (
                                    
                    this.state.userFollowing.map(following => (
                        <ClubFollowingList key={following.id} 

                            isFollow={false} 
                            dataFollowing={following} 
                            nama={following.club}
                            onFollow={state => this.handleFollow(state)}
                        />
                    ))
                )}
                {/* <ClubFollowingList nama="Club A" isFollow={false}/>
                <ClubFollowingList nama="Club B" isFollow={false}/>
                <ClubFollowingList nama="Club C" isFollow={false}/>
                <ClubFollowingList nama="Club D" isFollow={false}/>
                <ClubFollowingList nama="Club E" isFollow={false}/> */}
            </List>
        )
    }
}
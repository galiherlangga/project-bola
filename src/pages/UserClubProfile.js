import React from 'react';
import {StyleSheet, Image} from 'react-native';
import { Container, Header, Body, Title, Content, View, Text, Button, Icon, Tabs, Tab, Form, Input, Item,  } from 'native-base';
import ClubTimeline from './ClubTimeline';
import ClubFollowing from './ClubFollowing';
import ClubMedia from './ClubMedia';
import ClubProfile from './ClubProfile';
import {auth, db, storage} from '../../config/config'

export default class UserClubProfile extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            uid: '',
            users: [],
            text: '',
            timeLines: [],
            image: ''
        }

        this.uid =  auth.currentUser.uid;
    }

    componentDidMount = () => {
        this.getTimelines()
    }



    handleLogout = ({nav}) => {
        auth.signOut().then(()=>{
            nav.navigate('Auth')
        });
    }

    handleChange = key => val => {
        this.setState({[key]: val})
    }

    handleSubmit = () => {
        db.collection('timelines').doc().set({
            uid: this.uid,
            text: this.state.text
        }).then(()=>{
            this.getTimelines()
        })
    }

    getTimelines = () => {
        db.collection('timelines').where('uid', '==', this.uid).get()
        .then(res=> {
            return res.docs
        }).then(docs => {
            this.setState({
                timeLines: docs
            })
        })
    }

    render(){
        
        const {alamat, biodata, club, hp, players, web} = this.props.users

        return(
            <Container style={styles.container}>
                <Header style={{backgroundColor:'#000'}} androidStatusBarColor="#000">
                    <Body>
                        <Title>Profile</Title>
                    </Body>
                </Header>
                <Content>
                    <View style={styles.head}>
                        {/* <Image source={require('../../assets/avatar.png')} style={styles.avatar}/> */}
                        <Image source={{uri: this.props.image}} style={styles.avatar}/>
                        <View style={styles.headTextContainer}>

                            <View>
                                <Text style={styles.headText}>{club}</Text>
                                <Text style={styles.headText}>{web}</Text>
                                <Text style={styles.headText}>{alamat}</Text>
                            </View>

                            <View style={styles.headButtonContainer}>
                                <Button style={styles.button} transparent>
                                    <Icon name='md-calendar' style={styles.icon}/>
                                </Button>
                                <Button style={styles.button} onPress={()=>this.props.nav.navigate('Chat')} transparent>
                                    <Icon name='md-mail' style={styles.icon}/>
                                </Button>
                                <Button style={styles.button} onPress={()=>this.props.nav.navigate('EditProfile',{name:'Club',password:'password',email:'email@gmail.com'})} transparent>
                                    <Icon name='md-cog' style={styles.icon}/>
                                </Button>
                                <Button style={styles.button} transparent
                                    onPress={()=> this.handleLogout(this.props)}
                                >
                                    <Icon name='md-power' style={styles.icon}/>
                                </Button>
                            </View>
                        </View>
                    </View>
                    <Tabs>
                        <Tab heading="Timeline" tabStyle={styles.tabInactive} activeTabStyle={styles.tabActive} textStyle={styles.tabText}>
                            <Form style={styles.form}>
                                <Item style={styles.textArea} rounded>
                                    <Input onChangeText={this.handleChange('text')} placeholder='Biodata' multiline/>
                                    <Button onPress={this.handleSubmit} style={styles.button}>
                                        <Icon name='md-image'/>
                                        <Icon name='md-add'/>
                                    </Button>
                                </Item>
                            </Form>
                            <ClubTimeline timelines={this.state.timeLines} image={this.props.image} nama={club} self={true}/>
                        </Tab>
                        <Tab heading="Following" tabStyle={styles.tabInactive} activeTabStyle={styles.tabActive} textStyle={styles.tabText}>
                            <ClubFollowing/>
                        </Tab>
                        <Tab heading="Media" tabStyle={styles.tabInactive} activeTabStyle={styles.tabActive} textStyle={styles.tabText}>
                            <ClubMedia/>
                        </Tab>
                        <Tab heading="Profile" tabStyle={styles.tabInactive} activeTabStyle={styles.tabActive} textStyle={styles.tabText}>
                            <ClubProfile users={this.props.users} />
                        </Tab>
                    </Tabs>
                </Content>
            </Container>
        )
    }
}

const styles = StyleSheet.create(
    {
        container:{
            backgroundColor:'#fafafa'
        },
        head:{
            flex:0.2,
            flexDirection:'row',
            borderBottomColor: 'black',
            borderBottomWidth: 1,
        },
        avatar:{
            width:120,
            height:120,
            margin:10
        },
        headTextContainer:{
            flex:1,
            flexDirection:'column',
            margin:10
        },
        headText:{
            marginBottom:20,
            color:'#000'
        },
        headButtonContainer:{
            flex:1,
            flexDirection:'row',
            alignSelf:'flex-end',
        },
        button:{
            backgroundColor:'#000'
        },
        icon:{
            fontSize:25,
            color:'#fff'
        },
        tabInactive:{
            backgroundColor:'#fff'
        },
        tabActive:{
            backgroundColor:'#000'
        },
        tabText:{
            color:'#000'
        },
        form:{
            alignItems:'center',
            justifyContent:'center'
        },
        textArea:{
            backgroundColor:'#fafafa',
            marginVertical:10,
            marginLeft:10,
            marginRight:10,
            paddingHorizontal:10,
            paddingVertical:10,
            width:400,
            height:200,
            borderWidth:1,
            borderColor:'#424242',
            borderRadius:10,
            alignItems:'flex-start'
        }
    }
)
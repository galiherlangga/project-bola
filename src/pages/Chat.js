import React from 'react';
import {StyleSheet} from 'react-native';
import {Container, Header, Content, List, Text, Body, Title} from 'native-base';
import ChatList from './ChatList';


export default class Jadwal extends React.Component{
    static navigationOptions = {
        header: null,
    };
    render(){
        return(
            <Container style={styles.container}>
                <Header searchBar rounded style={{backgroundColor:'#000'}} androidStatusBarColor="#000" style={{display:'none'}}>
                    <Body>
                        <Title>Chat</Title>
                    </Body>
                </Header>
                <Content contentContainerStyle={styles.content}>
                    <List>
                        <ChatList/>
                        <ChatList/>
                        <ChatList/>
                        <ChatList/>
                        <ChatList/>
                        <ChatList/>
                        <ChatList/>
                        <ChatList/>
                    </List>
                </Content>
            </Container>
        )
    }
}
const styles = StyleSheet.create(
    {
        container:{
            backgroundColor:'#fff'
        },
        
    }
)
import React from 'react';
import { StyleSheet, Dimensions } from 'react-native';
import { Container, Header, Body, Title, Content, Thumbnail, Form, Item, Input, Button, Text } from 'native-base';

var {width,height}=Dimensions.get('window');

export default class EditProfile extends React.Component{
    static navigationOptions = {
        header: null,
    };
    constructor(props){
        super(props);
    }
    render(){
        const name = this.props.navigation.getParam('name','Name');
        const password = this.props.navigation.getParam('password','');
        const email = this.props.navigation.getParam('email','')
        return(
            <Container style={styles.container}>
                <Header style={{backgroundColor:"#ff6f00"}}>
                    <Body>
                        <Title>Edit Profile</Title>
                    </Body>
                </Header>
                <Content>
                    <Thumbnail source={require('../../assets/avatar.png')} style={styles.thumbnail}/>
                    <Form>
                        <Item>
                            <Input value={name}/>
                        </Item>
                        <Item>
                            <Input value={password} secureTextEntry/>
                        </Item>
                        <Item>
                            <Input value={email}/>
                        </Item>
                        <Button style={styles.button}>
                            <Text style={styles.textButton}>Simpan</Text>
                        </Button>
                    </Form>
                </Content>
            </Container>
        )
    }
}

const styles = StyleSheet.create(
    {
        container:{
            backgroundColor:'#fff'
        },
        thumbnail:{
            alignSelf:'center',
            width:150,
            height:150,
            marginTop:30,
            marginBottom:10 
        },
        button:{
            alignSelf:'center',
            alignItems:'center',
            justifyContent:'center',
            width:(width)-20,
            borderRadius:10,
            marginVertical:10,
            backgroundColor:'#000'
        },
        textButton:{
            alignItems:'center',
            color:'#fff'
        }
    }
)
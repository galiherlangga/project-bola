import React from 'react';
import {StyleSheet, View, Image} from 'react-native';
import {Container, Header, Content, Text, Button, Icon, Card, CardItem, Body, Title, Tabs, Tab} from 'native-base';
import ClubTimeline from './ClubTimeline';
import ClubFollowing from './ClubFollowing';
import ClubProfile from './ClubProfile';
import ClubMedia from './ClubMedia';
import {auth, db} from '../../config/config'

export default class DetailClub extends React.Component{
    static navigationOptions = {
        header: null
    };

    constructor(props){
        super(props)
        this.state = {
            isLoading: true,
            followed: false,
            clubData: null
        }

        this.uid = auth.currentUser.uid
        this.club_id = props.navigation.getParam('id')
    }

    componentDidMount = () => {

        this.getClubData()
        this.cekFollowing()
    }

    getClubData = () => {

        db.collection('users').doc(this.club_id).get()
            .then(async res => {

                this.setState(state => ({
                    clubData: res.data()
                }))
            })
    }

    handleFollow = () => {

        const following_id = this.props.navigation.getParam('id') //club id
        const uid = auth.currentUser.uid;

        db.collection('following').doc(uid).get().then(snap => {

            let isFollowed = this.state.followed[`${following_id}`]
            if(!snap.exists){
                db.collection('following').doc(uid).set({
                    [following_id]: true
                })
            }else{
                db.collection('following').doc(uid).update({
                    [following_id]: !isFollowed
                })
            }

            this.cekFollowing()
        }).catch(err => {
            alert(err.message)
        })
        
    }

    cekFollowing = () => {
        
        const uid = auth.currentUser.uid;
        const following_id = this.props.navigation.getParam('id') //club id

        db.collection('following').doc(uid).get().then(async snap => {
            
            if(snap.exists){
                await this.setState({followed: snap.data()})
            }
            
        }).then(()=> this.setState({isLoading: false}))
    }

    render(){

        if(this.state.isLoading){
            return null
        }
        // console.log(this.state.clubData)
        const {navigation} = this.props;
        const nama = navigation.getParam('nama','Nama Club');
        const following_id = this.props.navigation.getParam('id') //club id
        console.log(this.state.followed)
        
        return(
            <Container style={styles.container}>
                <Header style={{backgroundColor:'#000'}} androidStatusBarColor="#000">
                    <Body>
                        <Title>{nama}</Title>
                    </Body>
                </Header>
                <Content>
                    <View style={styles.head}>
                        <Image source={require('../../assets/avatar.png')} style={styles.avatar}/>
                        <View style={styles.headTextContainer}>
                            <Text style={styles.headText}>{this.state.clubData.club}</Text>
                            <Text style={styles.headText}>{this.state.clubData.web}</Text>
                            <Text style={styles.headText}>{this.state.clubData.alamat}</Text>
                            <View style={styles.headButtonContainer}>
                                {/* <Button style={styles.button}>
                                    <Text>Follow +</Text>
                                </Button> */}
                                <Button style={styles.button}>
                                    <Icon name='md-calendar' style={styles.icon}/>
                                </Button>
                                <Button style={styles.button}>
                                    <Icon name='md-chatboxes' style={styles.icon} onPress={()=>this.props.navigation.navigate('Chat')}/>
                                </Button>

                                <Button style={styles.button} onPress={this.handleFollow}>
                                    <Text>{this.state.followed[`${following_id}`] ? "Unfollow": "Follow"}</Text>
                                </Button>
                            </View>
                        </View>
                    </View>
                    <Tabs>
                        {/* <Tab heading="Timeline" tabStyle={styles.tab} activeTabStyle={{backgroundColor:'#000'}} textStyle={{color:'#000'}} activeTextStyle={{color:'#fff'}}>
                            <ClubTimeline nama={nama}/>
                        </Tab> */}
                        {/* <Tab heading="Following" tabStyle={styles.tab} activeTabStyle={{backgroundColor:'#000'}} textStyle={{color:'#000'}} activeTextStyle={{color:'#fff'}}>
                            <ClubFollowing/>
                        </Tab> */}
                        <Tab heading="Media" tabStyle={styles.tab} activeTabStyle={{backgroundColor:'#000'}} textStyle={{color:'#000'}} activeTextStyle={{color:'#fff'}}>
                            <ClubMedia/>
                        </Tab>
                        <Tab heading="Profile" tabStyle={styles.tab} activeTabStyle={{backgroundColor:'#000'}} textStyle={{color:'#000'}} activeTextStyle={{color:'#fff'}}>
                            <ClubProfile users={this.state.clubData} nama={this.props.nama}/>
                        </Tab>
                    </Tabs>
                </Content>
            </Container>
        )
    }
}
const styles = StyleSheet.create(
    {
        container:{
            backgroundColor:'#fff'
        },
        head:{
            flex:0.2,
            flexDirection:'row',
            borderBottomColor: 'black',
            borderBottomWidth: 1,
        },
        avatar:{
            width:120,
            height:120,
            margin:10
        },
        headTextContainer:{
            flex:1,
            flexDirection:'column',
            margin:10
        },
        headText:{
            marginBottom:20,
            color:'#000'
        },
        headButtonContainer:{
            flex:1,
            flexDirection:'row',
            alignSelf:'flex-end',
        },
        button:{
            backgroundColor:'#000'
        },
        icon:{
            fontSize:25,
            color:'#fff'
        },
        card:{
            marginTop:20,
            marginLeft:20,
            marginRight:20,
            borderRadius:25,
            backgroundColor:'transparent'
        },
        cardItem:{
            backgroundColor:'#c7c7c7'
        },
        bodyMember:{
            flex:1,
            flexDirection:'row',
        },
        viewCard:{
            flex:1,
        },
        tab:{
            backgroundColor:'#fff'
        }
    }
)
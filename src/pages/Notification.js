import React from 'react';
import {StyleSheet} from 'react-native';
import {Container, Header, Content, Body, Title, List} from 'native-base';
import NotificationList from '../component/NotificationList';

export default class Notification extends React.Component{
    render(){
        return(
            <Container style={styles.container}>
                <Header style={{backgroundColor:'#000'}} androidStatusBarColor="#000">
                    <Body>
                        <Title>Notification</Title>
                    </Body>
                </Header>
                <Content>
                    <List>
                        <NotificationList/>
                        <NotificationList/>
                        <NotificationList/>
                        <NotificationList/>
                        <NotificationList/>
                        <NotificationList/>
                    </List>
                </Content>
            </Container>
        )
    }
}

const styles = StyleSheet.create({
    container:{
        backgroundColor:'#fff'
    }
})
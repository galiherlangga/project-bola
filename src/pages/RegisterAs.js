import React from 'react';
import {StyleSheet, Image, Dimensions, TouchableHighlight} from 'react-native';
import {Container, Header, Content, Text, Button} from 'native-base';

var {width,height}=Dimensions.get('window')

export default class RegisterAs extends React.Component{
    render(){
        return(
            <Container style={styles.container}>
                <Header style={styles.header} androidStatusBarColor="#000" style={{display:'none'}}/>
                <Content contentContainerStyle={styles.content}>
                    <Image source={require('../../assets/logo.png')} style={styles.logo}/>
                    <Text style={styles.title}>DAFTAR SEBAGAI</Text>
                    <TouchableHighlight style={styles.button} rounded onPress={()=>this.props.navigation.navigate('RegisterUser')}>
                        <Text style={styles.text}>User</Text>
                    </TouchableHighlight>
                    <TouchableHighlight style={styles.button} rounded onPress={()=>this.props.navigation.navigate('RegisterClub')}>
                        <Text style={styles.text}>Official Club</Text>
                    </TouchableHighlight>
                    <Text onPress={()=>this.props.navigation.navigate('Login')}>Sudah punya akun? Login disini</Text>
                </Content>
            </Container>
        )
    }
}

const styles = StyleSheet.create(
    {
        container:{
            backgroundColor:'#fff'
        },
        header:{
            backgroundColor:'#fff',
            borderBottomWidth:0,
            shadowOffset: {
                height: 0, 
                width: 0
            }, 
            shadowOpacity: 0, 
            elevation: 0
        },
        content:{
            flex:1,
            alignItems:'center',
            justifyContent:'center',
        },
        logo:{
            width:200,
            height:200,
            margin:30
        },
        title:{
            fontSize:20,
            fontWeight:'bold'
        }, 
        button:{
            margin:10,
            padding:10,
            borderRadius:25,
            width:(width)-20,
            alignItems:'center',
            justifyContent:'center',
            backgroundColor:'#000'
        },
        text:{
            color:'#fff', 
            fontSize:20,
            fontWeight:'bold'
        }
    }
)
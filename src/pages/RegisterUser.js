import React from 'react';
import {StyleSheet, Image, Dimensions, TouchableHighlight} from 'react-native';
import {Container, Header, Content, Text, Form, Item, Input, Button, Body, Title, View} from 'native-base';
import DatePicker from '../component/DatePicker';
import {auth, db} from '../../config/config'

var {width,height}=Dimensions.get('window')
export default class RegisterUser extends React.Component{
    constructor(props){
        super(props)
        // console.ignoredYellowBox = [
        // 'Setting a timer'
        // ];
        this.state = {
            nama: '',
            email: '',
            password: '',
            password2: '',
            tanggal_lahir: '',
            hp: ''
        }
    }

    hendleChange = key => val =>{
        this.setState({[key]: val})
    }

    hendleRegister = (props, cred) => {
        const {nama, email, password, password2, tanggal_lahir, hp} = cred
        if(nama == ''){
            alert('nama wajib di isi')
            return 
        }
        if(email == ''){
            alert('email wajib di isi')
            return 
        }

        if(password == ''){
            alert('password wajib di isi')
            return 
        }

        if(password != password2){
            alert('password tidak sama')
            return
        }

        if(tanggal_lahir == ''){
            alert('tanggal lahir wajib di isi')
            return 
        }

        if(hp == ''){
            alert('no handphone wajib di isi')
            return 
        }
        
        auth.createUserWithEmailAndPassword(email, password).then(cred => {
            return db.collection('users').doc(cred.user.uid).set({
                nama: nama,
                role: 'user',
                tanggal_lahir: tanggal_lahir,
                email: email,
                hp: hp
            }).then(()=> {
                props.navigation.navigate('RegisterPhoto', {uid: cred.user.uid})
            })
        }).catch(err => {
            alert(err.message)
        });
        

    }

    data = (val) => {
        this.hendleChange('tanggal_lahir')(val)
    }
    render(){
        return(
            <Container style={styles.container}>
                <Header style={styles.header} androidStatusBarColor="#000" style={{display:'none'}}>
                    <Body>
                        <Title>Daftar Akun Baru</Title>
                    </Body>
                </Header>
                    <Content contentContainerStyle={styles.content} >
                        <Form style={styles.form}>
                            <Image source={require('../../assets/logo.png')} style={styles.logo}/>
                            <Item style={styles.item} rounded underline={false}>
                                <Input placeholder='Nama' style={styles.input}
                                    onChangeText={this.hendleChange('nama')}
                                />
                            </Item>
                            <Item style={styles.item} rounded underline={false}>
                                <Input placeholder='Email' style={styles.input}
                                    autoCapitalize='none'
                                    onChangeText={this.hendleChange('email')}
                                />
                            </Item>
                            <Item style={styles.item} rounded underline={false}>
                                <Input placeholder='Password' style={styles.input}
                                    autoCapitalize='none'
                                    secureTextEntry={true}
                                    onChangeText={this.hendleChange('password')}
                                />
                            </Item>
                            <Item style={styles.item} rounded underline={false}>
                                <Input placeholder='Ketik Ulang Password' style={styles.input}
                                    secureTextEntry={true}
                                    onChangeText={this.hendleChange('password2')}
                                    autoCapitalize='none'
                                />
                            </Item>
                            <DatePicker item={styles.item} rounded={true} input={styles.input} formatdate="DD-MM-YYYY" placeholder="Tanggal Lahir"
                                onHandleDate={val=> this.data(val)}
                            />
                            <Item style={styles.item} rounded underline={false}>
                                <Input placeholder='No Handphone' style={styles.input}
                                    onChangeText={this.hendleChange('hp')}
                                />
                            </Item>
                            <View style={styles.buttonContainer}>
                                <TouchableHighlight style={styles.button} rounded onPress={()=>{this.props.navigation.navigate('RegisterAs')}}>
                                    <Text style={styles.textButton}>KEMBALI</Text>
                                </TouchableHighlight>
                                <TouchableHighlight style={styles.button} rounded onPress={()=>{this.hendleRegister(this.props, this.state)}}>
                                    <Text style={styles.textButton}>LANJUTKAN</Text>
                                </TouchableHighlight>
                            </View>
                            
                        </Form>
                    </Content>
            </Container>
        )
    }
}

const styles = StyleSheet.create(
    {
        container:{
            backgroundColor:'#fff'
        },
        header:{
            backgroundColor:'#000',
            borderBottomWidth:0,
            shadowOffset: {
                height: 0, 
                width: 0
            }, 
            shadowOpacity: 0, 
            elevation: 0
        },
        content:{
            alignItems:'center',
            justifyContent:'center'
        },
        form:{
            alignItems:'center',
            justifyContent:'center'
        },
        logo:{
            width:100,
            height:100,
            margin:30
        },
        item:{
            backgroundColor:'#c7c7c7',
            marginVertical:10,
            marginLeft:10,
            marginRight:10,
            paddingHorizontal:10,
            width:(width)-20,
            borderBottomWidth:0
        },
        input:{
            color:'#000'
        },
        buttonContainer:{
            flexDirection:'row'
        },
        button:{
            margin:15,
            padding:15,
            borderRadius:25,
            flex:0.5,
            alignItems:'center',
            justifyContent:'center',
            backgroundColor:'#000'
        },
        textButton:{
            color:'#fff', 
            fontSize:10.95,
            fontWeight:'bold'
        }
    }
)
import React from 'react';
import {StyleSheet, Image, Dimensions, ActivityIndicator, View, TouchableHighlight} from 'react-native';
import {Container, Header, Content, Form, Item, Input, Button, Text } from 'native-base';
import {auth, db} from '../../config/config'

var {width,height}=Dimensions.get('window')
export default class Login extends React.Component{
    constructor(props){
        super(props)
        this.state = {
            email: '',
            password: '',
            isLoading: false
        }
    }

    handleChange = key => val =>{
        this.setState({[key]: val})
    }

    handleLogin = async (props, states) =>{

        await this.setState({isLoading: true})
        const {email, password} = states
        auth.signInWithEmailAndPassword(email, password).then(cred => {
            
            props.navigation.setParams({results: 'one'})
            props.navigation.navigate('Main', {uid: cred.user.uid})
            
        }).catch(async(err)=> {
            await this.setState({isLoading: false})
            alert(err.code)
        });
    }
    
    render(){
        
        if(this.state.isLoading){
            return (

                <View style={{backgroundColor: 'white', justifyContent: 'center', position: "absolute", top: 0, bottom: 0, left:0,right:0}}>
                    <ActivityIndicator size="large"/>
                </View>
            )
        }

        return(

            <Container style={styles.container}>
                <Header style={styles.header} androidStatusBarColor="#000" style={{display:'none'}}/>
                <Content contentContainerStyle={styles.content}>
                    <Form style={styles.form}>
                        <Image source={require('../../assets/logo.png')} style={styles.logo}/>
                        <Item style={styles.item} rounded underline={false}>
                            <Input placeholder="Email" style={styles.input}
                                autoCapitalize='none'
                                onChangeText={this.handleChange('email')}
                            />
                        </Item>
                        <Item style={styles.item} rounded underline={false}>
                            <Input placeholder="Password" style={styles.input} secureTextEntry
                                onChangeText={this.handleChange('password')}
                                autoCapitalize='none'
                            />
                        </Item>
                        <TouchableHighlight style={styles.button} rounded onPress={()=> this.handleLogin(this.props, this.state)}>
                            <Text style={styles.textButton}>LOGIN</Text>
                        </TouchableHighlight>
                        <Text onPress={()=>this.props.navigation.navigate('RegisterAs')}>Belum punya akun? Daftar disini</Text>
                    </Form>
                </Content>
            </Container>
        )
    }
}

const styles = StyleSheet.create(
    {
        container:{
            backgroundColor:'#fff'
        },
        header:{
            backgroundColor:'#fff',
            borderBottomWidth:0,
            shadowOffset: {
                height: 0, 
                width: 0
            }, 
            shadowOpacity: 0, 
            elevation: 0
        },
        content:{
            flex:1,
            alignItems:'center',
            justifyContent:'center',
        },
        form:{
            alignItems:'center',
            justifyContent:'center'
        },
        logo:{
            width:200,
            height:200,
            margin:30
        },
        item:{
            backgroundColor:'#c7c7c7',
            marginVertical:10,
            marginLeft:10,
            marginRight:10,
            paddingHorizontal:10,
            width:(width)-20,
            borderBottomWidth:0
        },
        input:{
            color:'#000'
        },
        button:{
            margin:10,
            padding:10,
            borderRadius:25,
            width:(width)-20,
            alignItems:'center',
            justifyContent:'center',
            backgroundColor:'#000'
        },
        textButton:{
            color:'#fff', 
            fontSize:20,
            fontWeight:'bold'
        }
    }
)
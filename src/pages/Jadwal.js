import React from 'react';
import {StyleSheet} from 'react-native';
import {Container, Header, Content, Text} from 'native-base';

export default class Jadwal extends React.Component{
    render(){
        return(
            <Container style={styles.container}>
                <Header/>
                <Content contentContainerStyle={styles.content}>
                    <Text>Jadwal</Text>
                </Content>
            </Container>
        )
    }
}
const styles = StyleSheet.create(
    {
        container:{
            backgroundColor:'#fff'
        },
        content:{
            flex:1,
            alignItems:'center',
            justifyContent:'center'
        }
    }
)
import React from 'react';
import {StyleSheet, View, Image, Dimensions} from 'react-native';
import {  } from 'native-base';

var images = [
    require('../../assets/highlight_1.jpg'),
    require('../../assets/highlight_2.jpg'),
    require('../../assets/highlight_3.jpg'),
    require('../../assets/highlight_1.jpg'),
    require('../../assets/highlight_2.jpg'),
    require('../../assets/highlight_3.jpg'),
]
var {width,height}=Dimensions.get('window')

export default class ClubMedia extends React.Component{
    render(){
        return(
            <View style={styles.rowView}>
                {this.renderSectionOne()}
            </View>
        )
    }
    renderSectionOne = () => {
        return images.map((image,index)=>{
            return(
                <View key={index} style={{width:(width)/3,height:(width)/3}}>
                    <Image style={{flex:1, width:undefined, height:undefined}} source={image}/>
                </View>
            )
        })
    }
}

const styles = StyleSheet.create({
    rowView:{
        flexDirection:'row',
        flexWrap:'wrap'
    }
})
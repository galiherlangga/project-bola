import React from 'react';
import {StyleSheet, Image, KeyboardAvoidingView, View, ActivityIndicator} from 'react-native';
import {Container, Header, Content, Text, Button, Form, Input, Item, Label, Body, Title} from 'native-base';
import UserNormalProfile from './UserNormalProfile';
import UserClubProfile from './UserClubProfile';
import {auth, db, storage} from '../../config/config'

export default class Jadwal extends React.Component{

    constructor(props){
        super(props)
        this.state = {
            uid: '',
            isLoading: true,
            role: '',
            users: [],
            image: ''
        }

        this.uid =  auth.currentUser.uid;
    }

    componentDidMount = async () => {

        const uid = auth.currentUser.uid
        await this.setState({uid: uid})

        await this.getData()

    }

    setData = (data) => {
        
        const image = storage.ref('images').child(data.image).getDownloadURL().then(res => {
            return res
        }).then(res => {
            
            this.setState({
                image: res,
                role: data.role,
                users: data,
            })
        })
    }

    getData = () => {

        db.collection('users').doc(this.uid).get().then(res => {
            return res.data();
            
        }).then(res => {
            this.setData(res)
        }).then(()=> this.setState({isLoading: false}))
    }

    static navigationOptions = {
        header: null,
    };
    render(){

        if(this.state.isLoading){
            return (

                <View style={{backgroundColor: 'white', justifyContent: 'center', position: "absolute", top: 0, bottom: 0, left:0,right:0}}>
                    <ActivityIndicator size="large"/>
                </View>
            )
        }


        return(

            <Container>
                {this.state.role == 'club' ? (
                    <UserClubProfile users={this.state.users} image={this.state.image} nav={this.props.navigation}/>

                ) : (
                    <UserNormalProfile users={this.state.users} nav={this.props.navigation}/>
                )}
            </Container>
                
        )
    }
}
const styles = StyleSheet.create(
    {
        container:{
            backgroundColor:'#fff'
        },
        content:{
            alignItems:'center',
            justifyContent:'center'
        },
        avatar:{
            width:150,
            height:150,
            margin:15,
        },
        button:{
            alignSelf:'center',
            backgroundColor:'#000',
            borderRadius:10,
            padding:20
        },
        profileText:{
            color:'#000',
            margin:15
        },
        input:{
            margin:10,
            padding:5,
            width:400
        },
        label:{
            color:'#000'
        }
    }
)
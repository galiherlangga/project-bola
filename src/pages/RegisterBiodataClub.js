import React from 'react';
import {StyleSheet, View, Dimensions, TouchableHighlight} from 'react-native';
import {Container, Header, Content, Text, Body, Form, Item, Input, Title, Picker, } from 'native-base';

import {db} from '../../config/config'

var {width,height}=Dimensions.get('window')

export default class RegisterBiodataClub extends React.Component{

    constructor(props){
        super(props)
        this.state = {
            isLoading: true,
            uid: '',
            biodata: '',
            hp: '',
            alamat: '',
            web: '',
            homebase: '',
            provinsi: '',
            kabupaten: '',
            arrprov: [],
            arrkabs: null,
            selectedProv: '',
            homebase: ''
        }
    }

    async componentDidMount(){
        await this.setUid()
        await this.getApi();
    }

    getApi = () => {

        //provinsi & kabupaten
        fetch('https://kodepos-2d475.firebaseio.com/list_propinsi.json?print=pretty')
            .then(res => (
                res.json()
            ))
            .then((jsonData) => {
                this.setState({arrprov: jsonData, isLoading: false})
                // console.log(this.state.arrprov)
            })

    }
    
    setUid = () => {
        const uid = this.props.navigation.getParam('uid')
        this.setState({uid: uid})
    }

    handleChange = key => val => {
        this.setState({
            [key]: val
        })
    }

    handleSubmit = state => {
        
        const {uid, biodata, hp, alamat, web, homebase, selectedKab, selectedProv} = state
        db.collection('users').doc(state.uid).update({
            biodata: biodata,
            hp: hp,
            alamat: alamat,
            web: web,
            homebase: homebase,
            provinsi: selectedProv,
            kabupaten: selectedKab
        }).then(()=> {
            this.props.navigation.navigate("RegisterAnggotaClub",{
                reganggota:0,
                uid: uid
            })
        }).catch(err => {
            alert(err.message)
        })
    }

    render(){
        if(this.state.isLoading){

            return null
        }
        
        console.log(this.state.selectedProv)
        return(
            <Container style={styles.container}>
                <Header style={styles.header} androidStatusBarColor="#000" style={{display:'none'}}>
                    <Body>
                        <Title>Biodata Club</Title>
                    </Body>
                </Header>
                <Content contentContainerStyle={styles.content}>
                    <Form style={styles.form}>
                        <Item style={styles.textArea} rounded>
                            <Input placeholder='Biodata' multiline
                                onChangeText={this.handleChange('biodata')}
                            />
                        </Item>
                        <Item style={styles.item} rounded>
                            <Input placeholder="No Telephone"
                                onChangeText={this.handleChange('hp')}
                            />
                        </Item>
                        <Item style={styles.item} rounded>
                            <Input placeholder="Alamat"
                                onChangeText={this.handleChange('alamat')}
                            />
                        </Item>
                        <Item style={styles.item} rounded>
                            <Input placeholder="Website"
                                onChangeText={this.handleChange('web')}
                            />
                        </Item>
                        <Item style={styles.item} rounded>
                            <Picker selectedValue={this.state.selectedProv && this.state.selectedProv}
                                onValueChange={(val, index) => {
                                    let data = val.split(':')
                                    this.setState({selectedProv: val})

                                    fetch(`https://kodepos-2d475.firebaseio.com/list_kotakab/${data[0]}.json?print=pretty`)
                                        .then(res => (
                                            res.json()
                                        )).then(jsonData => {
                                            this.setState({arrkabs: jsonData})
                                        })
                                }}
                            >
                                <Picker.item label="Select Provinsi" />
                                {this.state.arrprov && (
                                    Object.keys(this.state.arrprov).map(key => (
                                        <Picker.item label={this.state.arrprov[key]} key={key} value={`${key}:${this.state.arrprov[key]}`}/>
                                    ))
                                )}
                            </Picker>
                        </Item>

                        {this.state.arrkabs && (

                            <Item style={styles.item} rounded>
                                <Picker selectedValue={this.state.selectedKab && this.state.selectedKab}
                                    onValueChange={(val, index) => this.setState({selectedKab: val})}
                                >
                                    <Picker.item label="Select Kabupaten" />

                                    {Object.keys(this.state.arrkabs).map(key => (
                                            <Picker.item label={this.state.arrkabs[key]} key={key} value={this.state.arrkabs[key]} />
                                    ))}
                                </Picker>
                            </Item>
                        )}

                        <Item style={styles.item} rounded>
                            <Input placeholder="Homebase"
                                onChangeText={this.handleChange('homebase')}
                            />
                        </Item>
                        <View style={styles.buttonContainer}>
                            <TouchableHighlight style={styles.button} onPress={()=>this.props.navigation.navigate("RegisterClub")}>
                                <Text style={styles.textButton}>KEMBALi</Text>
                            </TouchableHighlight>
                            <TouchableHighlight style={styles.button} onPress={()=> this.handleSubmit(this.state)}>
                                <Text style={styles.textButton}>LANJUTKAN</Text>
                            </TouchableHighlight>
                        </View>
                    </Form>
                </Content>
            </Container>
        )
    }
}

const styles = StyleSheet.create(
    {
        container:{
            backgroundColor:'#fff'
        },
        header:{
            backgroundColor:'#000',
            borderBottomWidth:0,
            shadowOffset: {
                height: 0, 
                width: 0
            }, 
            shadowOpacity: 0, 
            elevation: 0
        },
        content:{
            alignItems:'center',
            justifyContent:'center'
        },
        form:{
            alignItems:'center',
            justifyContent:'center'
        },
        textArea:{
            backgroundColor:'#c7c7c7',
            marginVertical:10,
            marginLeft:10,
            marginRight:10,
            paddingHorizontal:10,
            paddingVertical:10,
            width:400,
            height:200,
            borderBottomWidth:0,
            alignItems:'flex-start'
        },
        item:{
            backgroundColor:'#c7c7c7',
            marginVertical:10,
            marginLeft:10,
            marginRight:10,
            paddingHorizontal:10,
            width:400,
            borderBottomWidth:0
        },
        buttonContainer:{
            flexDirection:'row',
            marginTop:100
        },
        button:{
            margin:15,
            padding:15,
            borderRadius:25,
            flex:0.5,
            alignItems:'center',
            justifyContent:'center',
            backgroundColor:'#000'
        },
        textButton:{
            color:'#fff', 
            fontSize:10.95,
            fontWeight:'bold'
        }
    }
)
import React from 'react';
import {StyleSheet, Text} from 'react-native';
import { Container, Header, Body, Title, Content, List, Card } from 'native-base';
import TimelineList from '../component/TimelineList';
import {db, auth} from '../../config/config'

export default class Timeline extends React.Component{

    constructor(props){
        super(props)
        this.state = {
            // isLoading: true,
            timelines: [],
            users: []
        }
        this.uid = auth.currentUser.uid
    }

    componentDidMount = () =>{
        this.getFollowed()
    }


    getFollowed = () => {

        db.collection('following').doc(this.uid).get()
            .then(res => {
                const identifiers = Object.keys(res.data())
                const active = identifiers.filter(function(id) {
                    return res.data()[id]
                })
                return active;
            }).then(res => {
                res.forEach((uid) => {
                    return db.collection('timelines').get()
                        .then(res=>{
                            return res.docs.map(function(item){
                                return item.data()
                            })
                        }).then(res => {
                            return res.filter(function(item){
                                return item.uid == uid
                            })
                            
                        }).then(datas => {

                            datas.forEach(item => {
                                this.getClubName(item)
                            })
                        })
                })

            })
    }

    getClubName = (data) => {
 
        db.collection('users').doc(data.uid).get()
            .then(res => {
                this.setState(state => ({
                    timelines: [...state.timelines, data],
                    users: [...state.users, res.data().club]
                }))
            })

    }
    render(){
        
        if(this.state.isLoading){
            return null
        }
        
        return(
            <Container style={styles.container}>
                <Header style={{backgroundColor:'#000'}} androidStatusBarColor="#000">
                    <Body>
                        <Title>Timeline</Title>
                    </Body>
                </Header>
                <Content >
                    {this.state.timelines != null && (
                        this.state.timelines.map((item, index) => {
                            return <TimelineList name={this.state.users[index]} caption={item.text}/>
                        })
                    )}
                    {/* <TimelineList name="Team B" caption="Hello" isImage={true}/>
                    <TimelineList name="Team C" caption="Ini status"/> */}
                </Content>
            </Container>
        )
    }
}

const styles = StyleSheet.create({
    container:{
        backgroundColor:'#fff'
    },
    content:{
        alignItems:'center',
        justifyContent:'center'
    }
})
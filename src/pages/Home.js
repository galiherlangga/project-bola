import React from 'react';
import {StyleSheet} from 'react-native';
import {Container, Header, Content, Text, Body, Title} from 'native-base';
import Slideshow from 'react-native-slideshow';
import HomeRow from '../component/HomeRow';

import {auth, db} from '../../config/config'

export default class Jadwal extends React.Component{
    static navigationOptions = {
        header: null,
        };
    
    constructor(props) {
        super(props)
        this.state = {
            isLoading: true,
            categories: ''
        }
    }
    componentDidMount = () => {

        this.getCategories()
    }

    getCategories = () => {

        db.collection('categories').orderBy('id', 'asc').get().then(res => {
            this.setState({isLoading: false, categories: res.docs})
        })
    }

    render(){
        if(this.state.isLoading){
            return null
        }
        return(
            <Container style={styles.container}>
                <Header style={{backgroundColor:'#000'}} androidStatusBarColor="#000">
                    <Body>
                        <Title>Highlight</Title>
                    </Body>
                </Header>
                <Content contentContainerStyle={styles.content}>
                <Slideshow 
                    dataSource={[
                      { url:require('../../assets/ssb1.jpeg')},
                      { url:require('../../assets/ssb2.jpeg')}
                    ]}
                />
                <HomeRow categories={this.state.categories} nav={this.props.navigation}/>
                </Content>
            </Container>
        )
    }
}
const styles = StyleSheet.create(
    {
        container:{
            backgroundColor:'#ffffff'
        },
        content:{
            flex:1,
            alignItems:'center',
        }
    }
)
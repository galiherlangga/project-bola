import React from 'react';
import {StyleSheet, Image, Text} from 'react-native';
import {Container, Header, Content } from 'native-base';

export default class Splash extends React.Component{
    render(){
        return(
            <Container style={styles.container}>
                <Header style={styles.header} androidStatusBarColor="#000" style={{display:'none'}}/>
                <Content contentContainerStyle={styles.content}>
                    <Image source={require('../../assets/logo.png')} style={styles.logo}/>
                </Content>
            </Container>
        )
    }
}

const styles = StyleSheet.create(
    {
        container:{
            backgroundColor:'#fff'
        },
        header:{
            backgroundColor:'#000',
            borderBottomWidth:0,
            shadowOffset: {
                height: 0, 
                width: 0
            }, 
            shadowOpacity: 0, 
            elevation: 0
        },
        content:{
            flex:1,
            alignItems:'center',
            justifyContent:'center'
        },
        logo:{
            width:100,
            height:100
        }
    }
)
import React from 'react';
import { StyleSheet, View } from 'react-native';
import { Card, CardItem, Text, Body, } from 'native-base';

export default class ClubProfile extends React.Component{
    render(){

        // const {alamat, biodata, club, hp, players, web, email, provinsi, kabupaten, homebase, category} = this.props.users
        let newprovinsi = ''
        if(this.props.users.provinsi){
            newprovinsi = this.props.users.provinsi.split(':')[1]
        }
        return(
            <>
                <Card>
                    <CardItem>
                        <Text>Biodata</Text>
                    </CardItem>
                    <CardItem>
                        <Body>
                            <Text>{this.props.nama}</Text>
                            <Text>{this.props.users.biodata}</Text>
                        </Body>
                    </CardItem>
                </Card>
                <Card>
                    <CardItem>
                        <Text>Informasi</Text>
                    </CardItem>
                    <CardItem>
                        <Body>
                            <Text>Provinsi : {newprovinsi || '-'}</Text>
                            <Text>Kabupaten : {this.props.users.kabupaten || '-'}</Text>
                            <Text>Alamat : {this.props.users.alamat || '-'}</Text>
                            <Text>Homebase : {this.props.users.homebase || '-'}</Text>
                            <Text>Category : {this.props.users.category || '-'}</Text>
                            <Text>Email : {this.props.users.email || '-'}</Text>
                            <Text>No. Telfon : {this.props.users.hp || '-'}</Text>
                            <Text>Web : {this.props.users.web || '-'}</Text>
                        </Body>
                    </CardItem>
                </Card>
                <Card>
                    <CardItem>
                        <Text>Anggota</Text>
                    </CardItem>
                    <CardItem>
                        <View style={styles.anggotaContainer}>
                            
                            {this.props.users.players && 
                                this.props.users.players.map(player => (
                                    <View style={styles.players} key={player.id}>
                                        <Text>{player.name}</Text>
                                    </View>
                                ))
                            }
                        </View>
                    </CardItem>
                </Card>
            </>
        )
    }
}

const styles = StyleSheet.create(
    {
        anggotaContainer:{
            flexDirection: 'row',
            flexWrap: 'wrap',
        },

        players: {
            width: '50%'
        }
    }
)
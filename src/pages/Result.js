import React from 'react';
import {StyleSheet} from 'react-native';
import {Container, Header, Content, Text, Body, Title, Form, Picker, Icon, List} from 'native-base';
import ResultList from '../component/ResultList';

export default class Result extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
          selected: undefined
        };
      }
      onValueChange(value: string) {
        this.setState({
          selected: value
        });
    }
    render(){
        return(
            <Container style={styles.container}>
                <Header searchBar rounded style={{backgroundColor:'#000'}} androidStatusBarColor="#000">
                    <Body>
                        <Title>Result</Title>
                    </Body>
                </Header>
                <Content contentContainerStyle={styles.content}>
                    <Picker
                        mode="dropdown"
                        iosHeader="Select your SIM"
                        iosIcon={<Icon name="arrow-down" />}
                        style={{ width: undefined }}
                        selectedValue={this.state.selected}
                        onValueChange={this.onValueChange.bind(this)}
                    >
                        <Picker.Item label="Competition" value="key0" />
                        <Picker.Item label="Liga 1 Shopee" value="key1" />
                        <Picker.Item label="Piala Presiden" value="key2" />
                        <Picker.Item label="Kratindeng Piala Indonesia" value="key3" />
                        <Picker.Item label="Trofeo Bali Island Cup" value="key4" />
                        <Picker.Item label="AFC Champion League" value="key5" />
                    </Picker>
                    <List>
                        <ResultList result="1 - 0" club1="Club A" club2="Club B"/>
                        <ResultList result="1 - 1" club1="Club C" club2="Club D"/>
                        <ResultList result="0 - 0" club1="Club E" club2="Club F"/>
                        <ResultList result="0 - 2" club1="Club G" club2="Club H"/>
                    </List>
                </Content>
            </Container>
        )
    }
}
const styles = StyleSheet.create(
    {
        container:{
            backgroundColor:'#fafafa'
        },
        picker:{
            width:400
        }
    }
)
import React from 'react';
import {StyleSheet, Dimensions, TouchableHighlight, View} from 'react-native';
import { Container, Header, Content, Button, Text, } from 'native-base';
import RenderImage from '../component/RenderImage'
import {ImagePicker} from 'expo'
import {auth, db, storage} from '../../config/config'

var {width,height}=Dimensions.get('window')
export default class RegisterPhoto extends React.Component{

    constructor(props){
        super(props)
        this.state = {
            photo: null,
            uid: '',
            update: ''
        }
    }

    componentDidMount(){
        const uid = this.props.navigation.getParam('uid')
        // const uid = 'ZJsHsITQ7qZ8LZEuufwDXxCSHSk1'
        this.setState({update: 'upup', uid: uid})
    }

    prevNav = (typeUser) => {
        if(typeUser==1){
            this.props.navigation.navigate("RegisterAnggotaClub",{reganggota:0})
        }else{
            this.props.navigation.navigate("RegisterUser")
        }
        console.log('tr')
        
    }

    handleChoose = async() => {
        try{
            const result = await ImagePicker.launchImageLibraryAsync()
            if(result.uri){
                this.setState({photo: result})
            }
        }catch(err){
            alert(err.message)
        }
    }
    
    handleSubmit = async ({photo}) => {

        try{

            const response = await fetch(photo.uri);
            const blob = await response.blob();
            
            let name = photo.uri.split('/')
            name = name.pop();
            
            const ref = storage.ref().child("images/"+name)
            ref.put(blob)

            db.collection('users').doc(this.state.uid).update({
                image: name
            }).then(()=>{
                this.props.navigation.navigate('Main', {uid: this.state.uid})
            })
            
        }catch(err){
            alert(err.message)
        }
    }
    
    render(){
        const {navigation} = this.props;
        const typeUser = navigation.getParam('typeUser',0);
        const {photo} = this.state

        return(
            <Container style={styles.container}>
                <Header style={styles.header} androidStatusBarColor="#000" style={{display:'none'}}/>
                <Content contentContainerStyle={styles.content}>
                    <RenderImage uri={!photo ? require('../../assets/avatar.png') : {uri: photo.uri}}/>
                    <TouchableHighlight style={styles.buttonUpload} rounded onPress={() => this.handleChoose()}>
                        <Text style={styles.textButton}>Upload</Text>
                    </TouchableHighlight>
                    
                    <View style={styles.buttonContainer}>
                        <TouchableHighlight style={styles.button} rounded onPress={()=>{this.props.navigation.goBack()}}>
                            <Text style={styles.textButton}>KEMBALI</Text>
                        </TouchableHighlight>
                        <TouchableHighlight style={styles.button} rounded onPress={()=> this.handleSubmit(this.state)}>
                            <Text style={styles.textButton}>LANJUTKAN</Text>
                        </TouchableHighlight>
                    </View>
                </Content>
            </Container>
        )
    }
}

const styles = StyleSheet.create(
    {
        container:{
            backgroundColor:'#fff'
        },
        header:{
            backgroundColor:'#fff',
            borderBottomWidth:0,
            shadowOffset: {
                height: 0, 
                width: 0
            }, 
            shadowOpacity: 0, 
            elevation: 0
        },
        content:{
            flex:1,
            alignItems:'center',
            // justifyContent:'center',
        },
        avatar:{
            width:200,
            height:200
        },
        button:{
            marginTop:15,
            margin:15,
            padding:15,
            borderRadius:25,
            flex:0.5,
            alignItems:'center',
            justifyContent:'center',
            backgroundColor:'#000'
        },
        buttonUpload:{
            margin:15,
            padding:15,
            borderRadius:25,
            width:180,
            alignItems:'center',
            justifyContent:'center',
            backgroundColor:'#000'
        },
        textButton:{
            color:'#fff', 
            fontSize:20,
            fontWeight:'bold'
        },
        buttonContainer:{
            flexDirection:'row',
            position: 'absolute',
            bottom: 0
        },
    }
)
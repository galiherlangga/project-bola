import React from 'react';
import {StyleSheet, Image, Dimensions, TouchableHighlight} from 'react-native';
import {Container, Header, Content, Body, Title, Form, Item, Input, View, Button, Text, Picker} from 'native-base';
import { auth, db } from '../../config/config';


var {width,height}=Dimensions.get('window')
export default class RegisterClub extends React.Component{

    constructor(props){
        super(props)
        this.state = {
            isLoading: true,
            club: '',
            email: '',
            password: '',
            password2: '',
            scategory: null
        }
    }

    componentDidMount = () => {

        this.getCategories()
    }

    getCategories = () => {

        db.collection('categories').orderBy('id', 'asc').get().then(res => {
            this.setState({isLoading: false, categories: res.docs})
        })
    }

    handleChange = key => val => {
        this.setState({
            [key]: val
        })
    }

    handleSubmit = (state) => {
        
        if(this.handleValidate(state)){
            
            auth.createUserWithEmailAndPassword(state.email, state.password).then(cred => {

                return db.collection('users').doc(cred.user.uid).set({
                    id: cred.user.uid,
                    role: 'club',
                    club: state.club,
                    email: state.email,
                    category_id: state.category_id,
                    category: state.scategory
                }).then(()=> {
                    this.props.navigation.navigate('RegisterBiodataClub', {uid: cred.user.uid})
                })
            }).catch(err => {
                alert(err.message)
            })
        }
        
    }

    handleValidate = (state) => {

        const {club, email, password, password2} = state
        if(club == ''){
            alert('club wajib diisi')
            return false
        }

        if(email == ''){
            alert('email wajib diisi')
            return false
        }

        if(password == ''){
            alert('password wajib diisi')
            return false
        }

        if(password != password2){
            alert('password tidak sama')
            return false
        }
        return true
    }

    render(){

        if(this.state.isLoading){
            return null
        }
        
        return(
            <Container style={styles.container}>
                <Header style={styles.header} androidStatusBarColor="#000" style={{display:'none'}}>
                    <Body>
                        <Title>Daftar Akun Baru</Title>
                    </Body>
                </Header>
                <Content contentContainerStyle={styles.content}>
                    <Form style={styles.form}>
                        <Image source={require('../../assets/logo.png')} style={styles.logo}/>
                        <Item style={styles.item} rounded>
                            <Input placeholder='Nama Club'  underline={false} style={styles.input}
                                onChangeText={this.handleChange('club')}
                            />
                        </Item>
                        <Item style={styles.item} rounded>
                            <Input placeholder='Email' underline={false} style={styles.input}
                                autoCapitalize='none'
                                onChangeText={this.handleChange('email')}
                            />
                        </Item>
                        <Item style={styles.item} rounded>
                            <Picker selectedValue={this.state.scategory && this.state.scategory}
                                onValueChange={(...val) => {
                                    console.log(val)
                                    this.setState({scategory: val[0], category_id: val[1]})
                                }}
                            >
                                <Picker.item label="Select Category" />
                                {this.state.categories && (
                                    this.state.categories.map(category => (
                                        <Picker.item label={category.data().name} key={category.data().id} value={category.data().id, category.data().name} />
                                    ))
                                )}
                            </Picker>
                        </Item>
                        <Item style={styles.item} rounded>
                            <Input placeholder='Password'  underline={false} style={styles.input}
                                autoCapitalize='none'
                                secureTextEntry={true}
                                onChangeText={this.handleChange('password')}
                            />
                        </Item>
                        <Item style={styles.item} rounded>
                            <Input placeholder='Ketik Ulang Password' underline={false} style={styles.input}
                                autoCapitalize='none'
                                secureTextEntry={true}
                                onChangeText={this.handleChange('password2')}
                            />
                        </Item>
                        <View style={styles.buttonContainer}>
                            <TouchableHighlight style={styles.button} rounded onPress={()=> this.props.navigation.navigate('RegisterAs')}>
                                <Text style={styles.textButton}>KEMBALI</Text>
                            </TouchableHighlight>
                            <TouchableHighlight style={styles.button} rounded onPress={()=>this.handleSubmit(this.state)}>
                                <Text style={styles.textButton}>LANJUTKAN</Text>
                            </TouchableHighlight>
                        </View>
                    </Form>
                </Content>
            </Container>
        )
    }
}

const styles = StyleSheet.create(
    {
        container:{
            backgroundColor:'#fff'
        },
        header:{
            backgroundColor:'#000',
            borderBottomWidth:0,
            shadowOffset: {
                height: 0, 
                width: 0
            }, 
            shadowOpacity: 0, 
            elevation: 0
        },
        content:{
            alignItems:'center',
            justifyContent:'center'
        },
        form:{
            alignItems:'center',
            justifyContent:'center'
        },
        logo:{
            width:100,
            height:100,
            margin:30
        },
        item:{
            backgroundColor:'#c7c7c7',
            marginVertical:10,
            marginLeft:10,
            marginRight:10,
            paddingHorizontal:10,
            width:(width)-20,
            borderBottomWidth:0
        },
        input:{
            color:'#000'
        },
        buttonContainer:{
            flexDirection:'row',
            marginTop:120
        },
        button:{
            margin:15,
            padding:15,
            borderRadius:25,
            flex:0.5,
            alignItems:'center',
            justifyContent:'center',
            backgroundColor:'#000'
        },
        textButton:{
            color:'#fff', 
            fontSize:10.95,
            fontWeight:'bold'
        }
    }
)
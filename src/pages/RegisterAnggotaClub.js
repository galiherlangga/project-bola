import React from 'react';
import {StyleSheet, View} from 'react-native';
import {Container, Header, Content, Body, Title, Form, Button, Text} from 'native-base';
import AddAnggotaClub from './../component/AddAnggotaClub';
import {auth, db} from '../../config/config'

export default class RegisterAnggotaClub extends React.Component{

    constructor(props){
        super(props)
        this.state = {
            uid: '',
            positions: [],
            players: [],
            p_1: {},
            p_2: {},
            p_3: {},
            p_4: {},
            p_5: {},
            p_6: {},
            p_7: {},
            p_8: {},
            p_9: {},
            p_10: {},
            p_11: {},
            p_12: {},
            p_13: {},
            p_14: {},
            p_15: {},
            p_16: {},
            p_17: {},
            p_18: {}
        }
    }

    componentDidMount(){

        // //set state positions
        // db.collection('positions').orderBy('id').get().then(snapshot => {

        //     let positions = snapshot.docs.map(doc =>{
        //         return doc.data()
        //     });

        //     this.setState({positions: positions})
        // })

        
        const uid = this.props.navigation.getParam('uid')
        this.setState({uid:uid})
    }

    nextNav = (reganggota) => {
        if(reganggota==1){
            this.props.navigation.navigate("RegisterPhoto",{typeUser:1})
        }else{
            this.props.navigation.navigate("RegisterAnggotaClub",{reganggota:1, players: this.state.players})
        }
        
    }

    getPlayer = async (data) => {
        
        let reganggota = this.props.navigation.getParam('reganggota');
        await this.setState({['p_'+data.id]: data})
        // console.log(this.state)
        const {uid} = this.state
        
        let players = []

        if(reganggota == 1){

            const prevPlayers = this.props.navigation.getParam('players')
            players = [
                ...prevPlayers,
                this.state.p_10,
                this.state.p_11,
                this.state.p_12,
                this.state.p_13,
                this.state.p_14,
                this.state.p_15,
                this.state.p_16,
                this.state.p_17,
                this.state.p_18,
            ]
        }else{
            players = [
                this.state.p_1,
                this.state.p_2,
                this.state.p_3,
                this.state.p_4,
                this.state.p_5,
                this.state.p_6,
                this.state.p_7,
                this.state.p_8,
                this.state.p_9,
            ]
        }

        await this.setState({players: players})
    }

    handleSubmit = () => {

        const reganggota = this.props.navigation.getParam('reganggota', 0)
        db.collection('users').doc(this.state.uid).update({
            players: this.state.players
        }).then(res => {
            this.nextNav(reganggota)
        }).catch(err => {
            console.log(err.codes)
        })
    }

    renderPlayer = () => {

        let i = 1;
        let sampai = 9
        if(this.props.navigation.getParam('reganggota') == 1) {
            i = 10 
            sampai = 18
        } 

        let players = []

        for(i; i <= sampai; i++){
            players.push(<AddAnggotaClub onGetPlayer={(data) => this.getPlayer(data)} id={i} key={i}/>)
        }

        return players
    }

    render(){

        const {navigation} = this.props;
        return(
            <Container style={styles.container}>
                <Header style={styles.header} androidStatusBarColor="#000" style={{display:'none'}}>
                    <Body>
                        <Title>Daftar Anggota Club</Title>
                    </Body>
                </Header>
                
                <Content contentContainerStyle={styles.content}>
                    <Form style={styles.form}>
                        {this.renderPlayer()}
                        {/* <AddAnggotaClub onGetPlayer={(data) => this.getPlayer(data)} id="1"/>
                        <AddAnggotaClub onGetPlayer={(data) => this.getPlayer(data)} id="2"/>
                        <AddAnggotaClub onGetPlayer={(data) => this.getPlayer(data)} id="3"/>
                        <AddAnggotaClub onGetPlayer={(data) => this.getPlayer(data)} id="4"/>
                        <AddAnggotaClub onGetPlayer={(data) => this.getPlayer(data)} id="5"/>
                        <AddAnggotaClub onGetPlayer={(data) => this.getPlayer(data)} id="6"/>
                        <AddAnggotaClub onGetPlayer={(data) => this.getPlayer(data)} id="7"/>
                        <AddAnggotaClub onGetPlayer={(data) => this.getPlayer(data)} id="8"/>
                        <AddAnggotaClub onGetPlayer={(data) => this.getPlayer(data)} id="9"/> */}
                        <View style={styles.buttonContainer}>
                            <Button style={styles.button} rounded onPress={()=>this.props.navigation.goBack()}>
                                <Text style={styles.textButton}>KEMBALI</Text>
                            </Button>
                            <Button style={styles.button} rounded onPress={this.handleSubmit}>
                                <Text style={styles.textButton}>LANJUTKAN</Text>
                            </Button>
                        </View>
                    </Form>
                </Content>
            </Container>
        )
    }
}



const styles = StyleSheet.create(
    {
        container:{
            backgroundColor:'#fff'
        },
        header:{
            backgroundColor:'#000',
            borderBottomWidth:0,
            shadowOffset: {
                height: 0, 
                width: 0
            }, 
            shadowOpacity: 0, 
            elevation: 0
        },
        content:{
            alignItems:'center',
            justifyContent:'center'
        },
        form:{
            alignItems:'center',
            justifyContent:'center',
            marginTop:10
        },
        buttonContainer:{
            flexDirection:'row',
        },
        button:{
            marginHorizontal:10,
            marginVertical:10,
            flex:0.5,
            alignItems:'center',
            justifyContent:'center',
            backgroundColor:'#000'
        },
        textButton:{
            color:'#fff', 
            fontSize:10.95,
            fontWeight:'bold'
        }
    }
)
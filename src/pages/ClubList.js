import React from 'react';
import {StyleSheet} from 'react-native';
import {Container, Header, Content, Text, List, Item, Icon, Input, Button} from 'native-base';
import ClubItemList from '../component/ClubItemList';
import {auth, db} from '../../config/config'

export default class ClubList extends React.Component{
    static navigationOptions = {
        header: null,
    };
    constructor(props){
        super(props);
        this.state = {
            isLoading: true,
            clubs: null,
            search: 'Per'
        }
        
        this.uid = auth.currentUser.uid
    }

    componentDidMount = () => {

        this.getClubs()
    }

    getClubs = () => {
        const uid = auth.currentUser.uid
        const {navigation} = this.props;
        const category_id = navigation.getParam('id')
        db.collection('users').where('category_id', '==', category_id).get().then(res => {

            return res.docs.map(function(item){
                return item.data()
            })
        }).then(res => {
            this.setState({clubs: res, isLoading: false, realclubs: res})
        })
    }

    handleChange = key => val => {
        console.log(val != '')
        this.setState({isLoading: true})
        if(val != ''){
            const filterdClubs = this.state.realclubs.map(club => {
                if(club.club.toLowerCase().indexOf(val.toLowerCase())  > -1 && club.id != this.uid){
                    return club
                }
                return null
            })
            this.setState({clubs: filterdClubs.filter(item => item), isLoading:false})
        }else{
            this.getClubs()
        }
        
    }

    render(){
        
        if(this.state.isLoading){
            return null
        }
        // console.log(this.state.clubs)
        const {navigation} = this.props;
        const category_id = navigation.getParam('id')

        return(
            <Container style={styles.container}>
                <Header searchBar rounded style={{backgroundColor:'#000'}} androidStatusBarColor="#000">
                    <Item>
                        <Icon name="md-search" />
                        <Input onChangeText={this.handleChange('search')} placeholder="Search" />
                        <Icon name="md-people" />
                    </Item>
                    {/* <Button transparent>
                        <Text>Search</Text>
                    </Button> */}
                </Header>
                <Content>
                    <List>
                        {this.state.clubs && (
                            this.state.clubs.map(club => {

                                if(club.id != this.uid){
                                    return (

                                        <ClubItemList key={club.id} 
                                            onPress={()=>navigation.navigate("DetailClub",{id: club.id, nama: club.club})}
                                            nav={navigation} 
                                            nama={club.club} 
                                            bio={club.bio}
                                            
                                        />
                                    )    
                                }
                            })
                        )}
                        {/* <ClubItemList onPress={()=>navigation.navigate("DetailClub",{nama:"Club B"})} nav={navigation} nama="Club B"/>
                        <ClubItemList onPress={()=>navigation.navigate("DetailClub",{nama:"Club C"})} nav={navigation} nama="Club C"/>
                        <ClubItemList onPress={()=>navigation.navigate("DetailClub",{nama:"Club D"})} nav={navigation} nama="Club D"/>
                        <ClubItemList onPress={()=>navigation.navigate("DetailClub",{nama:"Club E"})} nav={navigation} nama="Club E"/>
                        <ClubItemList onPress={()=>navigation.navigate("DetailClub",{nama:"Club F"})} nav={navigation} nama="Club F"/>
                        <ClubItemList onPress={()=>navigation.navigate("DetailClub",{nama:"Club G"})} nav={navigation} nama="Club G"/> */}
                    </List>
                </Content>
            </Container>
        )
    }
}
const styles = StyleSheet.create(
    {
        container:{
            backgroundColor:'#fff'
        }
    }
)
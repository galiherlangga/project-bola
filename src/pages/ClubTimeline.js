import React from 'react';
import {StyleSheet} from 'react-native';
import TimelineList from '../component/TimelineList';

export default class ClubTimeline extends React.Component{
    render(){
        return(
            <>
                {this.props.timelines && (
                    this.props.timelines.map((timeline, index) => (
                        <TimelineList name={this.props.nama} key={index} image={this.props.image} caption={timeline.data().text} self={this.props.self}/>
                    ))
                )}
                {/* <TimelineList name={this.props.nama} caption="ini caption" self={this.props.self}/>
                <TimelineList name={this.props.nama} self={this.props.self}/>
                <TimelineList name={this.props.nama} isImage={true} self={this.props.self}/> */}
            </>
        )
    }
}
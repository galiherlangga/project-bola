import React from "react";
import { 
    View,
    Text,
    Image,
    StyleSheet
} from "react-native";

const RenderImage = (props) => (
    <Image source={props.uri} style={styles.avatar}/>
)
export default RenderImage;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    avatar:{
        width:200,
        height:200
    },
});
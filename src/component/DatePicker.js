import React from 'react';
import {StyleSheet, TouchableOpacity, View} from 'react-native';
import {Icon, Item, Input} from 'native-base';
import DateTimePicker from 'react-native-modal-datetime-picker';
import Moment from 'moment';

export default class DatePicker extends React.Component{
    constructor(props){
        super(props);
        this.state={
            isDateVisible:false,
            choosenDate:''
        }
    }
    showDatePicker = () => {
        this.setState({isDateVisible:true});
    };
    hideDatePicker = (datetime) => {
        this.setState({
            isDateVisible:false,
            choosenDate:Moment(datetime).format(this.props.formatdate)
        }, () => {
            this.props.onHandleDate(this.state.choosenDate)
        });
    };
    handleDatePicker = () => {
        this.hideDatePicker();
    };



    render(){
        return(
            <View onHandleState="halo">
                <Item style={this.props.item} rounded={this.props.rounded} underline={false} onPress={()=>this.showDatePicker()}>
                    <Icon active name='md-calendar'/>   
                    <Input style={this.props.input} placeholder={this.props.placeholder} disabled value={this.state.choosenDate} />
                </Item>
                <DateTimePicker
                    isVisible={this.state.isDateVisible}
                    onConfirm={()=>this.handleDatePicker()}
                    onCancel={()=>this.hideDatePicker()}
                />
            </View>
        )
    }
}
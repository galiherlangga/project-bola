import React from 'react';
import {StyleSheet, View} from 'react-native';
import { ListItem, Left, Thumbnail, Body, Right, Text } from 'native-base';

export default class ResultList extends React.Component{
    render(){
        return(
            <ListItem style={styles.listItem}>
                <View>
                    <Text>Liga 1 Shopee</Text>
                    <Text>28 Mei 2019</Text>
                </View>
                <View style={styles.mainBody}>
                    <Left style={styles.leftRight}>
                        <Text>{this.props.club1}</Text>
                        <Thumbnail source={require('../../assets/kategori_1.jpeg')} style={styles.thumbnail}/>
                    </Left>
                    <Body style={styles.body}>
                        <Text>{this.props.result}</Text>
                    </Body>
                    <Right style={styles.leftRight}>
                        <Thumbnail source={require('../../assets/kategori_2.jpeg')} style={styles.thumbnail}/>
                        <Text>{this.props.club2}</Text>    
                    </Right>
                </View>
            </ListItem>
        )
    }
}

const styles = StyleSheet.create(
    {
        listItem:{
            flexDirection:'column'
        },
        mainBody:{
            flexDirection:'row',
            alignItems:'center'
        },
        leftRight:{
            flex:0.4,
            flexDirection:'row',
            alignItems:'center',
            justifyContent:'center'
        },
        body:{
            flex:0.2,
            flexDirection:'row',
            alignItems:'center',
            justifyContent:'center'
        },
        thumbnail:{
            margin:5
        }
    }
)
import React from 'react';
import {StyleSheet, View} from 'react-native';
import { ListItem, Thumbnail, Left, Body, Button,  Text} from 'native-base';
import {db, auth} from '../../config/config'

export default class ClubFollowingList extends React.Component{
    constructor(props){
        super(props);
        this.state={
            followText:'Follow +',
            isLoading: false
        }

        this.uid =  auth.currentUser.uid;
    }

    handleUnfollow = (club_id) => {

        this.setState(state => {

            if(state.isLoading == false){
                return {isLoading: true}
            }
        })

        db.collection('following').doc(this.uid)
            .update({
                [club_id]: false
            })
            .then(()=> this.setState({isLoading: false}))
        
        this.props.onFollow('klik')
        
    }
    render(){

        if(this.state.isLoading){
            return null
        }
        const {id, club} = this.props.dataFollowing
        return(
            <ListItem>
                <Left style={styles.left}>
                    <Thumbnail source={require('../../assets/avatar.png')} style={styles.thumbnail}/>
                </Left>
                <Body style={styles.body}>
                    <View style={styles.buttonContainer}>
                        <Text style={styles.text}>{club}</Text>
                        <Button rounded style={styles.button}
                            onPress={()=> this.handleUnfollow(id)}
                        >
                            <Text>{this.props.isFollow==true?'Follow +':'Unfollow'}</Text>
                        </Button>
                    </View>
                    <Text>xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx</Text>
                </Body>
            </ListItem>
        )
    }
}

const styles = StyleSheet.create(
    {
        left:{
            flex:0.3
        },
        thumbnail:{
            width:100,
            height:100
        },
        body:{
            flex:0.7
        },
        buttonContainer:{
            flexDirection:'row',
        },
        text:{
            flex:0.6
        },
        button:{
            alignItems:'flex-end',
            backgroundColor:'#000'
        }
    }
)
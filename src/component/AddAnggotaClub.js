import React from 'react';
import {StyleSheet, View} from 'react-native';
import {Item, Input, Picker, Icon} from 'native-base';

export default class AddAnggotaClub extends React.Component{
    constructor(props) {

        super(props);
        this.state = {
          selected: undefined,
          id: '',
          name: '',
          position: '',
          positions: [
              {id: 'key1', name: 'GK'},
              {id: 'key2', name: 'CB'},
              {id: 'key3', name: 'RB'},
              {id: 'key4', name: 'LB'},
              {id: 'key5', name: 'CMF'},
              {id: 'key6', name: 'CF'},
          ]
        };
    }

    async onValueChange(value:string){
        await this.setState({
            selected:value,
            position: value
        });

        this.handleGetPlayer(this.state);
    }

    handleChange = key => async (val) => {
        await this.setState((state, props) => ({
            id: props.id
        }));

        this.setState({[key]: val})
        
        this.handleGetPlayer(this.state);
    }

    handleGetPlayer = (state) => {
        const {id, name, position} = state
        this.props.onGetPlayer({id: id, name: name, position: position})
    }

    render(){
        return(
            <View style={styles.inputContainer}>
                <Item style={styles.itemInput} rounded>
                    <Input placeholder="Nama Pemain"
                        onChangeText={this.handleChange('name')}
                    />
                </Item>
                <Picker
                    mode="dropdown"
                    iosIcon={<Icon name="arrow-down"/>}
                    style={styles.itemPicker}
                    placeholder="Position"
                    selectedValue={this.state.selected}
                    onValueChange={this.onValueChange.bind(this)}
                >
                    <Picker.Item label="Position" value='key0'/>
                    {
                        this.state.positions.map(position => (
                            <Picker.Item label={position.name} value={position.id} key={position.id}/>
                        ))
                    }
                    {/* <Picker.Item label="Position" value='key0'/>
                    <Picker.Item label="GK" value='key1'/>
                    <Picker.Item label="RB" value='key2'/>
                    <Picker.Item label="LB" value='key3'/>
                    <Picker.Item label="CB" value='key4'/>
                    <Picker.Item label="CMF" value='key5'/>
                    <Picker.Item label="CF" value='key6'/> */}
                </Picker>
            </View>
        )
    }
}

const styles = StyleSheet.create(
    {
        inputContainer:{
            flexDirection:'row'
        },
        itemInput:{
            backgroundColor:'#fafafa',
            marginVertical:10,
            marginRight:10,
            marginLeft:10,
            paddingHorizontal:10,
            width:250,
            borderBottomWidth:0
        },
        itemPicker:{
            marginVertical:10,
            marginRight:10,
            paddingHorizontal:10,
        }
    }
)
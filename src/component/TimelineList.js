import React from 'react';
import {StyleSheet, Dimensions, Image} from 'react-native';
import { Left, Body, Thumbnail, Text, CardItem, Card, Right, Icon, Button, } from 'native-base';

var {width,height}=Dimensions.get('window')
export default class TimelineList extends React.Component{
    render(){
        return(
            <Card>
                <CardItem bordered>
                    <Left style={styles.left}>
                        {this.props.image ? <Thumbnail source={{uri: this.props.image}} style={styles.thumbnail}/> : <Thumbnail source={require('../../assets/avatar.png')} style={styles.thumbnail}/>}
                        
                    </Left>
                    <Body style={styles.body}>
                        <Text>{this.props.name}</Text>
                    </Body>
                    {this.editable()}
                </CardItem>
                {this.displayImage()}
                <CardItem footer>
                    <Text>{this.props.caption}</Text>
                </CardItem>
            </Card>
        )
    }

    displayImage(){
        if(this.props.isImage==true){
            return(
                <CardItem cardBody style={styles.cardBody} bordered>
                    <Image source={require('../../assets/highlight_1.jpg')} style={styles.image}/>
                </CardItem>
            )
        }
    }

    editable(){
        if(this.props.self==true){
            return(
                <Right style={styles.right}>
                    <Button transparent style={styles.button}>
                        <Icon name='md-settings' style={styles.icon}/>
                    </Button>
                    <Button transparent style={styles.button}>
                        <Icon name='md-trash' style={styles.icon}/>
                    </Button>
                </Right>
            )
        }
    }
}

const styles = StyleSheet.create({
    left:{
        flex:0.3
    },
    thumbnail:{
        width:100,
        height:100
    },
    body:{
        flex:0.6,
    },
    right:{
        flex:0.1,
        flexDirection:'row',        
    },
    button:{
        marginRight:10
    },
    icon:{
        color:'#000'
    },
    cardBody:{
        alignItems:'center',
        justifyContent:'center',
        
    },
    image:{
        width:(width)-40,
        height:(width)-40,
        marginTop:15,
        marginBottom:15
    }
})
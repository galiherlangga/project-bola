import React from 'react';
import {StyleSheet} from 'react-native';
import {ListItem, Left, Body, Thumbnail, Text} from 'native-base';

export default class ClubItemList extends React.Component{
    constructor(props){
        super(props);
    }
    render(){
        return(
            <ListItem onPress={this.props.onPress}>
                <Left style={styles.left}>
                    <Thumbnail square source={require('../../assets/avatar.png')} style={styles.thumbnail}/>
                </Left>
                <Body style={styles.body}>
                    <Text style={styles.textTitle}>{this.props.nama}</Text>
                    <Text style={styles.textSubtitle} note numberOfLines={3}>{this.props.bio}</Text>
                </Body>
            </ListItem>
        )
    }
}

const styles = StyleSheet.create(
    {
        thumbnail:{
            width:100,
            height:100
        },
        textTitle:{
            color:'#000'
        },
        textSubtitle:{
            color:'#000'
        },
        left:{
            flex:0.3
        },
        body:{
            flex:0.8
        }
    }
)
import React from 'react';
import {StyleSheet, Image, TouchableOpacity, Text} from 'react-native';
import { Button, View} from 'native-base';

export default class HomeRow extends React.Component{
    constructor(props){
        super(props);
    }
    render(){

        const navigation = this.props.nav;
        return(
            <View style={{ flexWrap: 'wrap', flexDirection: 'row', width: '100%', justifyContent: "space-between"}}>
                {this.props.categories && (
                    this.props.categories.map(category => (
                        <View key={category.id} style={{ width: '32%', alignItems: 'center', marginVertical: 20}}>

                            <TouchableOpacity onPress={()=>navigation.navigate('ClubList', {id: category.data().id})}>
                                <Image source={require('../../assets/kategori_1.jpeg')} style={styles.image}/>  
                            </TouchableOpacity>

                            <Text>{category.data().name}</Text>
                        </View>
                    ))
                )}
            </View>
        )
    }
}

const styles = StyleSheet.create(
    {
        view:{
            flexDirection:'row',
            marginVertical:10
        },
        button:{
            width:70,
            height:90,            
            margin:10,
            flex:1,
            alignItems:'center',
            justifyContent:'center'
        },
        text:{
            color:'#fff',
            fontSize:14
        },
        image:{
            width:80,
            height:80,
            borderRadius:25
        }
    }
)

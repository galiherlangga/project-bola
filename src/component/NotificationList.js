import React from 'react';
import {StyleSheet} from 'react-native';
import { ListItem, Left, Thumbnail, Icon, Body, Text } from 'native-base';

export default class NotificationList extends React.Component{
    render(){
        return(
            <ListItem>
                <Left style={styles.left}>
                    <Thumbnail source={require('../../assets/bell.png')} style={styles.iconLeft}/>
                </Left>
                <Body style={styles.body}>
                    <Text>Club A Mengajukan Tanding</Text>
                </Body>
            </ListItem>
        )
    }
}
const styles = StyleSheet.create(
    {
        left:{
            flex:0.3
        },
        iconLeft:{
            width:100,
            height:100
        },
        body:{
            flex:0.8
        }
    }
)
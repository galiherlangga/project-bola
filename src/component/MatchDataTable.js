import React from 'react';
import {StyleSheet, View} from 'react-native';
import {Thumbnail, Tab} from 'native-base';
import {Table, TableWrapper, Row, Cell} from 'react-native-table-component';

export default class MatchDataTable extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            tableHead:['POS', 'Team', 'GP', 'W', 'D', 'L', 'PTS'],
            tableData:[
                ['1', '2', '3', '4', '5', '6', '7'],
                ['1', '2', '3', '4', '5', '6', '7'],
                ['1', '2', '3', '4', '5', '6', '7'],
                ['1', '2', '3', '4', '5', '6', '7'],
                ['1', '2', '3', '4', '5', '6', '7']
            ],
            widthArr:[20, 100, 50, 50, 50, 50, 50]
        }
    }
    render(){
        const element = (data, index) => (
            <Thumbnail source={require('../../assets/avatar.png')} />
        );
        return(
            <View style={styles.container}>
                <Table borderStyle={{borderColor:'transparent'}}>
                    <Row data={this.state.tableHead} style={styles.head} textStyle={styles.text} widthArr={this.state.widthArr}/>
                    {
                        this.state.tableData.map((rowData, index)=>(
                            <TableWrapper key={index} style={styles.row}>
                                {
                                    rowData.map((cellData, cellIndex) => (
                                        <Cell key={cellIndex} data={cellIndex === 1 ? element(cellData,index):cellData} widthArr={this.state.widthArr}/>
                                    ))
                                }
                            </TableWrapper>
                        ))
                    }
                </Table>
            </View>
        )
    }
}

const styles = StyleSheet.create(
    {
        container: { 
            flex: 1, 
            padding: 16, 
            paddingTop: 30, 
            backgroundColor: '#fff' 
        },
        head:{
            height: 40, 
            backgroundColor: '#808B97'
        },
        text:{
            margin:10
        },
        row:{
            flexDirection:'row',
            backgroundColor:'#fff1c1'
        }
    }
)
import React from 'react';
import {StyleSheet, View} from 'react-native';
import { ListItem, Left, Thumbnail, Body, Right, Text, Icon } from 'native-base';

export default class UpcomingList extends React.Component{
    render(){
        return(
            <ListItem style={styles.listItem}>
                <View>
                    <Text>Liga 1 Shopee</Text>
                </View>
                <View style={styles.container}>
                    <Left style={styles.leftRight}>
                        <Text>{this.props.club1}</Text>
                        <Thumbnail source={require('../../assets/kategori_1.jpeg')} style={styles.thumbnail}/>
                    </Left>
                    <Body style={styles.body}>
                        <Text>VS</Text>
                    </Body>
                    <Right style={styles.leftRight}>
                        <Thumbnail source={require('../../assets/kategori_2.jpeg')} style={styles.thumbnail}/>
                        <Text>{this.props.club2}</Text>    
                    </Right>
                </View>
                <View style={styles.container}>
                    <Left style={styles.bottomLeft}>
                        <Icon name="md-calendar"/>
                    </Left>
                    <Body style={styles.bottomBody}>
                        <Text>1 Juni 2019</Text>
                    </Body>
                </View>
                <View style={styles.container}>
                    <Left style={styles.bottomLeft}>
                        <Icon name="md-map"/>
                    </Left>
                    <Body style={styles.bottomBody}>
                        <Text>Gor Candra Buana</Text>
                    </Body>
                </View>
            </ListItem>
        )
    }
}

const styles = StyleSheet.create(
    {
        listItem:{
            flexDirection:'column'
        },
        container:{
            flexDirection:'row',
            alignItems:'center'
        },
        leftRight:{
            flex:0.4,
            flexDirection:'row',
            alignItems:'center',
            justifyContent:'center'
        },
        body:{
            flex:0.2,
            flexDirection:'row',
            alignItems:'center',
            justifyContent:'center'
        },
        thumbnail:{
            margin:5
        },
        bottomLeft:{
            flex:0.2
        },
        bottomBody:{
            flex:0.8
        }

    }
)
import * as firebase from 'firebase'
import 'firebase/firestore'
import 'firebase/storage'

const firebaseConfig = {
    apiKey: "AIzaSyDk6ys4iFQaR0UtXtu5raqdYU618mGu44Q",
    authDomain: "bola-2a3c8.firebaseapp.com",
    databaseURL: "https://bola-2a3c8.firebaseio.com",
    projectId: "bola-2a3c8",
    storageBucket: "bola-2a3c8.appspot.com",
    messagingSenderId: "434602537173",
    appId: "1:434602537173:web:d419593a7be6770e"
};

firebase.initializeApp(firebaseConfig)

const auth = firebase.auth()
const db = firebase.firestore()
const storage = firebase.storage()
export {auth, db, storage}
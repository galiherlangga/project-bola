import React from 'react';
import {createAppContainer, createSwitchNavigator, createBottomTabNavigator, createStackNavigator, createMaterialTopTabNavigator} from 'react-navigation';
import { Font } from 'expo';
import { Ionicons } from '@expo/vector-icons';
import { Icon } from 'native-base';
//Splash
import Splash from './src/pages/Splash';
//Auth
import Login from './src/pages/Login';
import RegisterAs from './src/pages/RegisterAs';
import RegisterUser from './src/pages/RegisterUser';
import RegisterClub from './src/pages/RegisterClub';
import RegisterBiodataClub from './src/pages/RegisterBiodataClub';
import RegisterAnggotaClub from './src/pages/RegisterAnggotaClub';
import RegisterAnggotaClub2 from './src/pages/RegisterAnggotaClub';
import RegisterPhoto from './src/pages/RegisterPhoto';
//Main
import Home from './src/pages/Home';
import Result from './src/pages/Result';
import Chat from './src/pages/Chat';
import Profile from './src/pages/Profile';
import Timeline from './src/pages/Timeline';
//Detail
import ClubList from './src/pages/ClubList';
import DetailClub from './src/pages/DatailClub';
import Upcoming from './src/pages/Upcoming';
import MatchTable from './src/pages/MatchTable';
import Notification from './src/pages/Notification';
import EditProfile from './src/pages/EditProfile';

const Auth = createStackNavigator(
    {
        Login:Login,
        RegisterAs:RegisterAs,
        RegisterUser:RegisterUser,
        RegisterClub:RegisterClub,
        RegisterBiodataClub:RegisterBiodataClub,
        RegisterAnggotaClub:RegisterAnggotaClub,
        RegisterAnggotaClub2:RegisterAnggotaClub2,
        RegisterPhoto:RegisterPhoto
    },{
        // initialRouteName: 'RegisterPhoto',
        headerMode:'none',
        navigationOptions:{
            headerVisible:false
        }
    }
)

const Main = createBottomTabNavigator(
    {
        Home:{
            screen:createStackNavigator({
                Highlight:Home,
                ClubList:ClubList,
                DetailClub:DetailClub,
                Chat:Chat
            }),
            navigationOptions:{
                tabBarIcon:()=>(
                    <Icon name='md-home' style={{color:'#fafafa'}}/>
                )
            }
        },
        Timeline:{
            screen:Timeline,
            navigationOptions:{
                tabBarIcon:()=>(
                    <Icon name='md-megaphone' style={{color:'#fafafa'}}/>
                )
            }
        },
        Jadwal:{
            screen:createMaterialTopTabNavigator({
                Result:Result,
                Upcoming:Upcoming,
            }),
            navigationOptions:{
                tabBarIcon:()=>(
                    <Icon name='md-trophy' style={{color:'#fafafa'}}/>
                )
            }
        },
        Notification:{
            screen:Notification,
            navigationOptions:{
                tabBarIcon:()=>(
                    <Icon name='md-notifications' style={{color:'#fafafa'}}/>
                )
            }
        },
        Profile:{
            screen:createStackNavigator({
                Profiles:Profile,
                EditProfile:EditProfile
            }),
            navigationOptions:{
                tabBarIcon:()=>(
                    <Icon name='md-person' style={{color:'#fafafa'}}/>
                )
            }
        },
    },{
        // initialRouteName: 'Home',
        tabBarOptions:{
            style:{backgroundColor:'#424242'},
            showLabel:false
        }
    }
)


const AppNavigator = createAppContainer(createSwitchNavigator(
    {
        Auth:Auth,
        Main: Main,
    },
    // {initialRouteName: 'Main'}
))

export default class App extends React.Component {
    static navigationOptions = { header: null }
  
    constructor(props){
      super(props);
    
      this.state = {
        isAppReady: false
      }
    }
  
    async componentDidMount() {
      await Font.loadAsync({
        'Roboto': require('./node_modules/native-base/Fonts/Roboto.ttf'),
        'Roboto_medium': require('./node_modules/native-base/Fonts/Roboto_medium.ttf'),
        ...Ionicons.font,
      });
      this.setState({ isAppReady: true });
    }
  
    render() {
      if(!this.state.isAppReady){
        return <Splash />;
      }
  
      return <AppNavigator/>
    }
  }